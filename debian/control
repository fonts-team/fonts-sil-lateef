Source: fonts-sil-lateef
Section: fonts
Priority: optional
Maintainer: Debian Fonts Task Force <debian-fonts@lists.debian.org>
Uploaders: Nicolas Spalinger <nicolas.spalinger@sil.org>,
           Daniel Glassey <wdg@debian.org>,
           Hideki Yamane <henrich@debian.org>,
Build-Depends: debhelper-compat (= 13),
Standards-Version: 4.6.2
Homepage: https://software.sil.org/lateef/
Vcs-Git: https://salsa.debian.org/fonts-team/fonts-sil-lateef.git
Vcs-Browser: https://salsa.debian.org/fonts-team/fonts-sil-lateef
Rules-Requires-Root: no

Package: fonts-sil-lateef
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}
Description: OpenType Arabic font for Sindhi and other languages of South Asia
 Lateef is named after Shah Abdul Lateef Bhitai, the famous Sindhi
 mystic and poet. It is intended to be an appropriate style for use
 in Sindhi and other languages of the South Asian region.
 .
 This font provides a simplified rendering of Arabic script, using basic
 connecting glyphs but not including a wide variety of additional ligatures
 or contextual alternates (only the required lam-alef ligatures). This
 simplified style is often preferred for clarity, especially in non-Arabic
 languages, but may be considered unattractive in more traditional and
 literate communities.
 .
 This release supports virtually all of the Unicode 5.0 Arabic character
 repertoire (excluding the Arabic Presentation Forms blocks, which are not
 recommended for normal use). Font smarts are implemented using OpenType
 technology.
